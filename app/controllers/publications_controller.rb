class PublicationsController < ApplicationController  
  before_action :set_publication, only: %i[update show destroy edit]
  def create 
    publication = Publication.create(publication_params)

    redirect_to publication_path(publication), notice: 'Вы создали блог!'
  end

  def update 
    @publication.update(publication_params)

    redirect_to publication_path(@publication), notice: 'Вы сохранили блог!'
  end

  def destroy 
    @publication.destroy

    redirect_to publication_path, notice: 'Вы удалили блог!'
  end

  def show
  end

  def index 
    @publication = Publication.all
  end

  def new 
    @publication = Publication.new
  end

  def edit 
  end

  private

  def publication_params 
    params.require(:publication).permit(:body, :post_title)
  end

  def set_publication 
    @publication = Publication.find(params[:id])
  end
end
