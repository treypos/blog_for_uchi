class CreatePublications < ActiveRecord::Migration[7.0]
  def change
    create_table :publications do |t|
      t.text :body
      t.text :post_title, index: true

      t.timestamps
    end
  end
end
