7) Необязательная, но выполнение будет очень большим плюсом
a)Напишите простой блог на рельсе с минимальным функционалом (один автор,
который выкладывает посты. Комментарии, сортировки, фильтры и основные рюшечки
не обязательны, но остаются на ваше усмотрение и желание. Как и стилизация)
Heroku:
https://blog.heroku.com/deploying-react-with-zero-configuration
Вместо Heroku, можно использовать любой другой хостинг

blog for uchi.ru

##### 1. Prerequisites

The setups steps expect following tools installed on the system.

* Ruby (2.7.0) (with [rvm](https://rvm.io/) for example)
* [Node.js](https://nodejs.org/en/download/)
* [Yarn](https://classic.yarnpkg.com/en/docs/install/)
* Rails [7.0.3](https://github.com/rails/rails/tree/v7.0.3)

##### 2. Installing bundler and dependencies:

```bash
gem install bundler
```

```bash
bundle install
```
##### 3. Prepare the database:

```bash
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed
```
##### 4. Start the Rails server:

```ruby
bundle exec rails s
```

And now you can visit the site with the URL http://localhost:3000